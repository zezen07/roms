<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('admin.dashboard', [
    	'title' => "Dasboard"
    ]);
});
Route::resource('/nutrisi/lahan', 'Admin\LahanController');
Route::resource('/nutrisi/sensor', 'Admin\SensorController');
Route::resource('/nutrisi/hasil-akhir', 'Admin\HasilAkhirController');
