<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHasilAkhirTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hasil_akhir', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lahan_id')->unsigned();;
            $table->decimal('sensor_keasaman')->nullable();
            $table->string('sensor_kelembapan')->nullable();
            $table->string('sensor_warna')->nullable();
            $table->string('takaran_keasaman')->nullable();
            $table->string('takaran_air')->nullable();
            $table->string('takaran_pupuk')->nullable();
            $table->timestamps();

            $table->foreign('lahan_id')
                ->references('id')->on('lahan')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hasil_akhir');
    }
}
