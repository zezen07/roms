<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferensiPupukTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referensi_pupuk', function (Blueprint $table) {
            $table->increments('id');
            $table->string('warna_tanah');
            $table->decimal('sebelum_tanam');
            $table->decimal('fase_vegetative');
            $table->decimal('fase_generative');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('referensi_pupuk');
    }
}
