<?php

use Illuminate\Database\Seeder;
use App\ReferensiPupuk;
class ReferensiPupukSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ReferensiPupuk::create([
        	'warna_tanah' => 'KELABU',
	    	'sebelum_tanam' => '20000',
	    	'fase_vegetative' => '50',
	    	'fase_generative' => '45'
        ]);
        ReferensiPupuk::create([
        	'warna_tanah' => 'MERAH',
	    	'sebelum_tanam' => '22500',
	    	'fase_vegetative' => '55',
	    	'fase_generative' => '50'
        ]);
        ReferensiPupuk::create([
        	'warna_tanah' => 'KUNING',
	    	'sebelum_tanam' => '25000',
	    	'fase_vegetative' => '60',
	    	'fase_generative' => '55'
        ]);

      //   ReferensiPupuk::create([
      //   	'warna_tanah' => 'KELABU HITAM',
	    	// 'sebelum_tanam' => '{"takaran_pupuk":20000.00,"keterangan":"Pupuk Kandang"}',
	    	// 'fase_vegetative' => '{"takaran_pupuk":50.00,"keterangan":"Pupuk Urea"}',
	    	// 'fase_generative' => '[{"id":1,takaran_pupuk":60.00,"keterangan":"Pupuk Urea"},{"id":2,"takaran_pupuk":45.00,"keterangan":"Pupuk NPK"}]'
      //   ]);
      //   ReferensiPupuk::create([
      //   	'warna_tanah' => 'KELABU HITAM',
	    	// 'sebelum_tanam' => '{"takaran_pupuk":22500.00,"keterangan":"Pupuk Kandang"}',
	    	// 'fase_vegetative' => '{"takaran_pupuk":55.00,"keterangan":"Pupuk Urea"}',
	    	// 'fase_generative' => '[{"id":1,"takaran_pupuk":66.67,"keterangan":"Pupuk Urea"},{"id":2,"takaran_pupuk":50.00,"keterangan":"Pupuk NPK"}]'
      //   ]);
      //   ReferensiPupuk::create([
      //   	'warna_tanah' => 'KELABU HITAM',
	    	// 'sebelum_tanam' => '{"takaran_pupuk":25000.00,"keterangan":"Pupuk Kandang"}',
	    	// 'fase_vegetative' => '{"takaran_pupuk":60.00,"keterangan":"Pupuk Urea"}',
	    	// 'fase_generative' => '[{"id":1,"takaran_pupuk":73.37,"keterangan":"Pupuk Urea"},{"id":2,"takaran_pupuk":5 5.00,"keterangan":"Pupuk NPK"}]'
      //   ]);
    }
}
