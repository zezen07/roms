<?php

use Illuminate\Database\Seeder;
use App\ReferensiKapur;
class ReferensiKapurSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$selisih_ph = 0.0;
    	$takaran = 505;

    	for ($i=0; $i < 38 ; $i++) { 	
    		ReferensiKapur::create([
	        	'selisih_ph' => $selisih_ph,
	        	'takaran' => $takaran
	        ]);
	        $selisih_ph = $selisih_ph + 0.1;
	        $takaran = $takaran + 315;
    	}
    }
}
