<?php

use Illuminate\Database\Seeder;
use App\ReferensiAir;

class ReferensiAirSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ReferensiAir::create([
        	'kelembapan' => 'KERING',
        	'takaran_air' => '720',
        ]);
        ReferensiAir::create([
        	'kelembapan' => 'LEMBAP',
        	'takaran_air' => '480',
        ]);
        ReferensiAir::create([
        	'kelembapan' => 'BASAH',
        	'takaran_air' => '0',
        ]);
    }
}
