<?php

use Illuminate\Database\Seeder;
use App\Lahan;

class LahanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Lahan::create([
        	'luas_lahan' => '1000',
        	'keterangan' => 'pak solihin'
        ]);
    }
}
