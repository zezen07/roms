<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Lahan;
use App\HasilAkhir;

class LahanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = 'Data Lahan';
        $data['data'] = Lahan::all();
        return view('admin.lahan.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['title'] = "Data Lahan Baru";
        return view('admin.lahan.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->session()->has('lahan_id')) {
            $request->session()->forget('lahan_id');
        }
        
        try {
            $validate = Validator::make($request->all(),[
                'luas_lahan' => 'required|min:3|numeric',
                'keterangan' => 'required|min:3'
            ]);

            if ($validate->fails()) {
                 return redirect(route('lahan.create'))->withErrors($validate, 'lahan')
                        ->withInput();
            }

            $createLahan = Lahan::create($request->all());
            if ($createLahan) {
                $getLahan = Lahan::orderBy('id','desc')->first();
                $request->session()->put('lahan_id',$getLahan->id);
                
                return redirect(route('sensor.create'));
            }

        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
