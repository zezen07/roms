<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\HasilAkhir;
use App\lahan;
use App\ReferensiAir;
use App\ReferensiKapur;
use App\ReferensiPupuk;

class HasilAkhirController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = "Data Hasil Akhir";
        $data['data'] = HasilAkhir::with('lahan')->get();
        return view('admin.hasil-akhir.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.hasil-akhir.create',['title' => "Lanjutkan proses Pencarian takaran nutrisi"]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $getLahanId = 2;
        $getLahanId = $request->session()->get('lahan_id');
        
        if ($getLahanId) {
            $getSensor = HasilAkhir::select([
                'hasil_akhir.sensor_keasaman as keasaman',
                'hasil_akhir.sensor_kelembapan as kelembapan',
                'hasil_akhir.sensor_warna as warna',
                'hasil_akhir.lahan_id as lahan_id',
            ])->join('lahan','lahan.id','=','lahan_id')
            ->where('lahan_id',$getLahanId)
            ->first();

            //Pencarian Takaran Air
            if ($getSensor['kelembapan'] && $getSensor['warna']) {
                $getTakaranAir = ReferensiAir::Select([
                    'referensi_air.kelembapan as kelembapan',
                    'referensi_air.takaran_air as takaran_air'
                ])->where('kelembapan',$getSensor['kelembapan'])->first();

                if ($getTakaranAir['takaran_air'] != 0) {
                    if ($getSensor['warna'] == "KELABU") {
                        $takaranAir = [
                            'jumlah_air' => $getTakaranAir['takaran_air'],
                            'keterangan' => "Disiram 2 hari Sekali"
                        ]; 
                    }
                    if ($getSensor['warna'] == "MERAH") {
                        $takaranAir = [
                            'jumlah_air' => $getTakaranAir['takaran_air'] + 100,
                            'keterangan' => "Disiram 2 hari Sekali"
                        ];
                    }
                    if ($getSensor['warna'] == "KUNING") {
                        $takaranAir = [
                            'jumlah_air' => $getTakaranAir['takaran_air'] + 200,
                            'keterangan' => "Disiram 2 hari Sekali"
                        ];
                    }
                }elseif ($getTakaranAir['takaran_air'] == 0){
                    $takaranAir = [
                        'jumlah_air' => $getTakaranAir['takaran_air'],
                        'keterangan' => "Bedengan sedalam 10-20 cm"
                    ];
                }
            }

            //Pencarian Takaran Kapur
            if ($getSensor['keasaman'] && $getSensor['warna']) {
                if ($getSensor['keasaman'] <= 6.5 && $getSensor['keasaman'] > 0) {
                    $selisihPh = 6.5 - $getSensor['keasaman'];
                    $keterangan = "KAPUR";
                }elseif ($getSensor['keasaman'] > 6.5 && $getSensor['keasaman'] <= 14) {
                    $selisihPh = $getSensor['keasaman'] - 6.5;
                    $keterangan = "BELERANG";
                }
                $selisihPh = number_format((float)$selisihPh, 2, '.', '');
                $getReferensiKapur = ReferensiKapur::where('selisih_ph',$selisihPh)->first();
                $getLahan = Lahan::where('id',$getLahanId)->first();
                // return $getLahan;
                if ($getReferensiKapur) {
                    if ($getSensor['warna']="KELABU") {
                        $takaranPh = ($getLahan->luas_lahan * $getReferensiKapur->takaran)/10000;
                    }
                    if ($getSensor['warna']="MERAH") {
                        $takaranPh = ($getLahan->luas_lahan * ($getReferensiKapur->takaran+200))/10000;    
                    }
                    if ($getSensor['warna']="KUNING") {
                        $takaranPh = ($getLahan->luas_lahan * ($getReferensiKapur->takaran+400))/10000;    
                    }
                    $takaranKeasaman = [
                        'takaranPh' => $takaranPh,
                        'keterangan' => $keterangan,
                    ];
                }
            }

            //Pencarian Pupuk
            if ($getSensor['warna']) {
                $getReferensiPupuk = ReferensiPupuk::where('warna_tanah',$getSensor['warna'])->first();
                $sebelumTanam = ($getLahan->luas_lahan * $getReferensiPupuk->sebelum_tanam)/10000;
                $faseVegetative = ($getLahan->luas_lahan * $getReferensiPupuk->fase_vegetative)/10000;
                $npkGenerative = ($getLahan->luas_lahan * $getReferensiPupuk->fase_generative)/10000;
                $ureaGenerative = (4 * $npkGenerative)/3;

                $takaranPupuk = [
                    'sebelum_tanam' => $sebelumTanam,
                    'fase_vegetative' => $faseVegetative,
                    'npk_generative' => $npkGenerative,
                    'urea_generative' => $ureaGenerative
                ];
            }
            $HasilAkhir = HasilAkhir::where('lahan_id',$getLahanId)->update([
                'takaran_air' => json_encode($takaranAir),
                'takaran_keasaman' => json_encode($takaranKeasaman),
                'takaran_pupuk' => json_encode($takaranPupuk)
            ]);

            return redirect(route('hasil-akhir.index'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
