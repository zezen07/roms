<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\HasilAkhir;
use App\lahan;

class SensorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = "Data Sensor";
        $data['data'] = HasilAkhir::with('lahan')->get();
        // return $data;
        return view('admin.sensor.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.sensor.create',['title' => "Masukkkan Nilai Sensor"]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sessionLahanId = $request->session()->get('lahan_id');
        $getLahanId = HasilAkhir::where('lahan_id', $sessionLahanId)->first();
        if (!$getLahanId) {
            $deteksiSensor = HasilAkhir::create([
                'lahan_id' => $sessionLahanId,
                'sensor_kelembapan' =>$request->sensor_kelembapan,
                'sensor_keasaman' =>$request->sensor_keasaman,
                'sensor_warna' =>$request->sensor_warna,
            ]);
        }
        $deteksiSensor = HasilAkhir::where('lahan_id',$sessionLahanId)
        ->update([
            'sensor_kelembapan' =>$request->sensor_kelembapan,
            'sensor_keasaman' =>$request->sensor_keasaman,
            'sensor_warna' =>$request->sensor_warna,
        ]);

        return redirect(route('hasil-akhir.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
