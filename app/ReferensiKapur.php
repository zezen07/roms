<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReferensiKapur extends Model
{
	protected $table = "referensi_kapur";
	protected $primarykey = "id";
	protected $fillable= [
		'selisih_ph',
		'takaran'
	];
}
