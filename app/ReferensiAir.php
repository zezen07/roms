<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReferensiAir extends Model
{
    protected $table = "referensi_air";
    protected $primarykey = "id";
    protected $fillable = [
    	'kelembapan',
    	'takaran_air'
    ];
}
