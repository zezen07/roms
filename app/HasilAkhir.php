<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HasilAkhir extends Model
{
    protected $table = "hasil_akhir";
    protected $primarykey = "id";
    protected $fillable = [
    	'lahan_id',
    	'sensor_keasaman',
    	'sensor_kelembapan',
    	'sensor_warna',
    	'takaran_keasaman',
    	'takaran_kelembapan',
    	'takaran_warna',
    ];

    public function lahan()
    {
    	return $this->belongsTo(Lahan::class);
    }
}
