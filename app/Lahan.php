<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lahan extends Model
{
    //
    protected $table = "lahan";
    protected $primarykey = "id";
    
    protected $fillable = [
    	'luas_lahan',
    	'keterangan'
    ];
}
