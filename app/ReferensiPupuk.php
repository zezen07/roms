<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReferensiPupuk extends Model
{
    protected $table = "referensi_pupuk";
    protected $primarykey = "id";
    protected $fillable = [
    	'warna_tanah',
    	'sebelum_tanam',
    	'fase_vegetative',
    	'fase_generative'
    ];
}
