<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<!-- Memanggil file meta didalam folder includes -->
	@include('includes.meta')
	<title>{{ config('app.name','Laravel') }}</title>
	<!-- Memanggil file css didalam folder includes -->
	@include('includes.css')
</head>
<body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">
	<div class="site-wrap">
		@include('partials.topbar_section')
		<!-- Memanggil file header section partials -->
		@include('partials.header_section')
		<!-- Content -->
		@yield('content')
		<!-- Memanggil file footer section partials -->
		@include('partials.footer_section')
		<!-- Memanggil file js pada folderincludes -->
	</div>
	{{-- <script src="../../assets/default-template/js/animate.js"></script>
    <script src="../../assets/default-template/js/custom.js"></script> --}}
    @include('includes.js')
    @include('includes.js_public')
    <script src="//unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	@yield('scripts')
	@if(\Session::has('message'))
		<script type="text/javascript">
			swal("{{\Session::get('message')}}");
		</script>
	@endif
	<script>
		jQuery('#loginButton').click(function() {
			jQuery.ajax({
				url : "{{ url('check_login') }}",
				data : jQuery('.loginForm').serialize(),
				success : function(response) {
					alert(response)
				},
				error : function(response){
					alert(response)
				}
			});
		});
	</script>
</body>
</html>