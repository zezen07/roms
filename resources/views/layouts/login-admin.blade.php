<!DOCTYPE html>
<html>
<head>
	<title>Login Administrator</title>
	@include('includes.admin.meta')
	@include('includes.admin.css')
</head>
<body>
  <div class="accountbg"></div>
  <div class="wrapper-page">
      @yield('content')
  </div>
	@include('includes.admin.js')
</body>
</html>