<!DOCTYPE html>
<html>
	<head>
		@include('includes.admin.meta')
		<title>{{ $title }}</title>
		@include('includes.admin.css')

		@yield('styles')
		
	</head>
	<body>
<body class="fixed-left">
	<div id="wrapper">
		@include('includes.admin.topbar')
		@include('includes.admin.side-menu')
		{{-- @include('webadmin.includes.topbar')
		@include('webadmin.includes.side-menu') --}}
		<div class="content-page">
		    <div class="content">
		        <div class="">
		            <div class="page-header-title">
		                <h4 class="page-title">{{ $title }}</h4>
		            </div>
		        </div>
		        <div class="page-content-wrapper ">
		            <div class="container">
		            	<div class="row">
		            		<div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
		            			<div class="panel panel-primary">
		            				<div class="panel-body">
		            					@yield('content')
		            				</div>
		            			</div>
		            		</div>
		            	</div>
		            </div>
		        </div>
		    </div>
		    <footer class="footer"> © {{ date('Y') }} WebAdmin - All Rights Reserved. </footer>
		</div>
	</div>
		@include('includes.admin.js')
		<script src="//unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		@yield('scripts')
		
		@if(\Session::has('message'))
			<script type="text/javascript">
				swal("{{\Session::get('message')}}");
			</script>
		@endif
	</body>
</html>
