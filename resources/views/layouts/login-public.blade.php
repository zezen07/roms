<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    {{-- Memanggil file meta di dalam folder includes --}}
    @include('includes.meta')
    <title>{{ config('app.name','Laravel') }}</title>
    {{-- Memanggil File Csss Di dalam Folder includes --}}
    @include('includes.css')

</head>

<body>
	@include('partials.topbar_section')
	{{-- Memanggi Partial File  Header Section --}}
	@include('partials.header_section')
	{{-- Memanggi Partial File Minicart Wrap --}}
	{{-- @include('partials.mini-cart-wrap') --}}
	<!-- Cart Overlay -->
	<div class="cart-overlay"></div>
	{{-- Memanggi Partial File Page Banner --}}
	{{-- @include('partials.page-banner') --}}
	{{-- Ini adalah konten --}}
	@yield('content')
	<!-- Memanggil file footer section partials -->
	@include('partials.footer_section')
	{{-- Memanggil File Javascript di dalam folder includes --}}
	@include('includes.js')
	{{-- Content Javascript --}}
	@yield('scripts')

</body>

</html>
