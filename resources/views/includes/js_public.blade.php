    
    <script src="{{asset('assets/default-template/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('assets/default-template/js/jquery-ui.js')}}"></script>
    <script src="{{asset('assets/default-template/js/popper.min.js')}}"></script>
    <script src="{{asset('assets/default-template/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/default-template/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('assets/default-template/js/jquery.stellar.min.js')}}"></script>
    <script src="{{asset('assets/default-template/js/jquery.countdown.min.js')}}"></script>
    <script src="{{asset('assets/default-template/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('assets/default-template/js/jquery.easing.1.3.js')}}"></script>
    <script src="{{asset('assets/default-template/js/aos.js')}}"></script>
    <script src="{{asset('assets/default-template/js/jquery.fancybox.min.js')}}"></script>
    <script src="{{asset('assets/default-template/js/jquery.sticky.js')}}"></script>
    <script src="{{asset('assets/default-template/js/main.js')}}"></script>
    <script src="{{asset('assets/default-template/js/bootstrap-input-spinner.js')}}"></script>
    {{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script> --}}