	<link href="https://fonts.googleapis.com/css?family=Muli:300,400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/default-template/fonts/icomoon/style.css')}}">

    <link rel="stylesheet" href="{{asset('assets/default-template/css/bootstrap/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('assets/default-template/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/default-template/css/jquery-ui.css')}}">
    <link rel="stylesheet" href="{{asset('assets/default-template/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/default-template/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/default-template/css/owl.theme.default.min.css')}}">

    <link rel="stylesheet" href="{{asset('assets/default-template/css/jquery.fancybox.min.css')}}">

    <link rel="stylesheet" href="{{asset('assets/default-template/css/bootstrap-datepicker.css')}}">

    <link rel="stylesheet" href="{{asset('assets/default-template/fonts/flaticon/font/flaticon.css')}}">
    
    <link rel="stylesheet" href="{{asset('assets/default-template/css/aos.css')}}">
    
    <link rel="stylesheet" href="{{asset('assets/default-template/css/style.css')}}">

    <link href="//stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
