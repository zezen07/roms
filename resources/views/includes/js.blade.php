<script type="text/javascript" src="{{ asset('assets/default-template/js/vendor/jquery-1.12.4.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/default-template/js/vendor/modernizr-2.8.3.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/default-template/js/popper.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/default-template/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/default-template/js/plugin.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/default-template/js/ajax-mall.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/default-template/js/main.js') }}"></script>