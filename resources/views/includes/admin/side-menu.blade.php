@php
  $colorArray = ['00cec9','2d3436','d63031','fdcb6e','6c5ce7'];
@endphp
<div class="left side-menu">
  <div class="sidebar-inner slimscrollleft">
    <div class="user-details">
      <div class="text-center">
        <img src="https://ui-avatars.com/api/?background={{ $colorArray[rand(0,4)] }}&color=fff&name=zezen" alt="" class="img-circle">
      </div>
      <div class="user-info">
        <div class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            zezen
          </a>
        </div>
      </div>
    </div>
    <div id="sidebar-menu">
      <ul>
        <li>
          <a href="javascript:void(0)">
            <i class="mdi mdi-home"></i>
            <span>
              Beranda
            </span>
          </a>
        </li>
      {{-- Menu Data Master --}}
        <li class="has_sub">
          <a href="javascript:void(0)">
            <i class="mdi mdi-database"></i>
            <span>
              Data Nutrisi
            </span>
            
            <span class="pull-right">
              <i class="mdi mdi-plus"></i>
            </span>
          </a>

          <ul class="list-unstyled">
            <li>
              <a href="{{ route('lahan.index') }}">
                Data Lahan
              </a>
            </li>
            <li>
              <a href="{{ route('sensor.index') }}">
                Data Sensor
              </a>
            </li>
            <li>
              <a href="{{ route('hasil-akhir.index') }}">
                Data Akhir
              </a>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  <div class="clearfix"></div>
  </div>
</div>