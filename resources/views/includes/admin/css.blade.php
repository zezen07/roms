<link rel="stylesheet" type="text/css" href="{{ asset('assets/admin-template/themes/bootstrap/css/bootstrap.min.css') }}">

<link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/ionicons/4.4.1/collection/icon/icon.css" />
<link href="//cdn.materialdesignicons.com/1.1.34/css/materialdesignicons.min.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" type="text/css" href="{{ asset('assets/admin-template/themes/webadmin/css/style.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/admin-template/plugins/DataTables/DataTables-1.10.18/css/dataTables.bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
