@php
  $colorArray = ['00cec9','2d3436','d63031','fdcb6e','6c5ce7'];
@endphp
<div class="topbar">
    <div class="topbar-left">
        <div class="text-center">
            <a href="" class="logo"><span>Web</span>Admin</a>
            <a href="javascript:void(0)" class="logo-sm"><span>W</span></a>
        </div>
    </div>
    <div class="navbar navbar-default" role="navigation">
        <div class="container">
            <div class="">
                <div class="pull-left">
                    <button type="button" class="button-menu-mobile open-left waves-effect waves-light">
                        <i class="mdi mdi-menu"></i>
                    </button>
                    <span class="clearfix"></span>
                </div>

                <ul class="nav navbar-nav navbar-right pull-right">
                    <li class="hidden-xs">
                        <a href="javascript:void(0)" id="btn-fullscreen" class="waves-effect waves-light notification-icon-box">
                            <i class="mdi mdi-fullscreen"></i>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="" class="dropdown-toggle profile waves-effect waves-light" data-toggle="dropdown" aria-expanded="true">
                            <img src="https://ui-avatars.com/api/?background={{ $colorArray[rand(0,4)] }}&color=fff&name=zezen" alt="" class="img-circle">
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="javascript:void(0)">
                                    Profil
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    Pengaturan
                                </a>
                            </li>
                            
                            <li class="divider"></li>
                            <li>
                                <a href="{{ url('/') }}">
                                    Keluar
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
