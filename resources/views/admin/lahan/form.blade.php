<div class="row">

	<div class="col-md-12">
		<div class="form-group">
			<label class="col-md-2 control-label" for="luas_lahan">Luas Lahan</label>
			<div class="col-md-10">
				{{ Form::text('luas_lahan', null, ['class' => 'form-control','id' => 'luas_Lahan','placeholder' => 'Luas Lahan']) }}
			</div>
		</div>
		<div style="padding-bottom:50px;"></div>
	</div>

	<div class="col-md-12">
		<div class="form-group">
			<label class="col-md-2 control-label" for="keterangan">Nama Pemilik</label>
			<div class="col-md-10">
				{{ Form::text('keterangan', null, ['class' => 'form-control','id' => 'nama_pemilik','placeholder' => 'Nama Pemilik']) }}
			</div>
		</div>
		<div style="padding-bottom:50px;"></div>
	</div>
	
	<div class="col-md-12">
    <button type="submit" class="btn btn-primary btn-sm pull-right">
      <i class="fa fa-save"></i> Simpan
    </button>
    <div style="padding-bottom:20px;"></div>
	</div>

</div>