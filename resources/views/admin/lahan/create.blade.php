@extends('layouts.dashboard')

@section('styles')
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css" />
@endsection

@section('content')

  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      {!! Form::open([
          'url' => route('lahan.store'),
          'method' => 'POST',
          'enctype' => 'multipart/form-data'
          ])
      !!}
      {!! csrf_field() !!}
        @include('admin.lahan.form')
      {!! Form::close() !!}
    </div>
  </div>

@endsection
@section('scripts')
  <script src="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.min.js"></script>
  <script type="text/javascript">
    jQuery(document).ready(function() {
      jQuery('#description').summernote({
        height: 300,
      });
    });
  </script>
@endsection