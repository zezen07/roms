@extends('layouts.dashboard')

@section('styles')
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css" />
  {{-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/admin-template/plugins/datatables/DataTables-1.10.18/css/dataTables.bootstrap.min.css') }}"> --}}

@endsection

@section('content')

  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      {!! Form::model($data, ['url' => route('categories.update',$data->id), 'method' => 'PUT','enctype'=>'multipart/form-data']) !!}
      {!! csrf_field() !!}
        @include('admin.categories.form')
      {!! Form::close() !!}
    </div>
  </div>

@endsection

@section('scripts')
  <script src="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.min.js"></script>
  <script type="text/javascript">
    jQuery(document).ready(function() {
      jQuery('#description').summernote({
        height: 300,
      });
    });
  </script>
@endsection