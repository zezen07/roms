<div class="row">

	<div class="col-md-12">
		<div class="form-group">
			<label class="col-md-2 control-label" for="name">Nama Category</label>
			<div class="col-md-10">
				{{ Form::text('name', null, ['class' => 'form-control','id' => 'name','placeholder' => 'Nama Category']) }}
			</div>
		</div>
		<div style="padding-bottom:50px;"></div>
	</div>
	
	<div class="col-md-12">
    <button type="submit" class="btn btn-primary btn-sm pull-right">
      <i class="fa fa-save"></i> Simpan
    </button>
    <div style="padding-bottom:20px;"></div>
	</div>

</div>