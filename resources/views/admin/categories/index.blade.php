@extends('layouts.dashboard')
@section('content')

  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <p>
            <a href="{{ route('categories.create') }}" class="btn btn-primary btn-sm">
                <i class="fa fa-plus">New</i>
            </a>    
        </p>
        
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">

      <table class="table category_datatable">

        <thead>
          <tr>
            <th class="text-center">No</th>
            <th class="text-center">Name Category</th>
            <th class="text-center">Action</th>
          </tr>
        </thead>

        <tbody>
          
          @php
            $no= 1;
          @endphp


          @foreach($data as $row)
          <tr class="text-center">
            <td class="text-center">{{$no}}</td>
            <td class="text-center">{{$row->name}}</td>
            <td class="text-center">
              <a href="{{ route('categories.edit',$row->id) }}" class="btn btn-primary btn-sm">
                <i class="fa fa-edit"></i>
              </a>
                
              {!! Form::open(array(
                  'route'=>array('categories.destroy',$row->id),
                  'method'=>'DELETE',
                  '_token'=> csrf_token(),
                  'style' => 'display:inline')) !!}
    
                  <button class="btn btn-danger btn-sm" type="submit"><i class="fa fa-trash"></i></button>
              {!!Form::close()!!}
                
            </td>
          </tr>

          @php
            $no++;
          @endphp

          @endforeach

        </tbody>  

      </table>
    </div>
  </div>

@endsection

@section('scripts') 

  <script type="text/javascript">
    jQuery('.category_datatable').DataTable();
  </script>

@endsection

