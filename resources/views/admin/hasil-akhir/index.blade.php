@extends('layouts.dashboard')
@section('content')

  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <p>
            <a href="{{ route('hasil-akhir.create') }}" class="btn btn-primary btn-sm">
                <i class="fa fa-plus">New</i>
            </a>    
        </p>
        
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">

      <table id="hasil_akhir_datatable" class="table table-striped dt-responsive nowrap dataTable no-footer dtr-inline collapsed" style="width:100%;">

        <thead>
          <tr>
            <th class="text-center">No</th>
            <th class="text-center">Nama Pemilik</th>
            <th class="text-center">Takaran Air</th>
            <th class="text-center">Takaran Kapur</th>
            <th class="text-center">Takaran Pupuk</th>
            <th class="text-center">Action</th>
          </tr>
        </thead>

        <tbody>
          
          @php
            $no= 1;
          @endphp


          @foreach($data as $row)
          <tr class="text-center">
            <td class="text-center">{{$no}}</td>
            <td class="text-center">{{$row->lahan->keterangan}}</td>
            <td class="text-center">{{$row->takaran_air}}</td>
            <td class="text-center">{{$row->takaran_keasaman}}</td>
            <td class="text-center">{{$row->takaran_pupuk}}</td>
            <td class="text-center">
              <a href="{{ route('hasil-akhir.edit',$row->id) }}" class="btn btn-primary btn-sm">
                <i class="fa fa-edit"></i>
              </a>
                
              {!! Form::open(array(
                  'route'=>array('hasil-akhir.destroy',$row->id),
                  'method'=>'DELETE',
                  '_token'=> csrf_token(),
                  'style' => 'display:inline')) !!}
    
                  <button class="btn btn-danger btn-sm" type="submit"><i class="fa fa-trash"></i></button>
              {!!Form::close()!!}
                
            </td>
          </tr>

          @php
            $no++;
          @endphp

          @endforeach

        </tbody>  

      </table>
    </div>
  </div>

@endsection

@section('scripts') 

  <script type="text/javascript">
    jQuery('#hasil_akhir_datatable').DataTable();
  </script>

@endsection

