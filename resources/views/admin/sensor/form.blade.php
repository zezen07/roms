<div class="row">

	<div class="col-md-12">
		<div class="form-group">
			<label class="col-md-2 control-label" for="sensor_keasaman">Sensor Keasaman</label>
			<div class="col-md-10">
				{{ Form::text('sensor_keasaman', null, ['class' => 'form-control','id' => 'sensor_keasaman','placeholder' => 'Sensor Keasaman']) }}
			</div>
		</div>
		<div style="padding-bottom:50px;"></div>
	</div>

	<div class="col-md-12">
		<div class="form-group">
			<label class="col-md-2 control-label" for="sensor_kelembapan">Sensor Kelembapan</label>
			<div class="col-md-10">
				{{ Form::select('sensor_kelembapan', ['KERING' => 'Kering', 'LEMBAB' => 'Lembab', 'BASAH' => 'Basah'], null, ['class' => 'form-control','id' => 'sensor_kelembapan','placeholder' => 'Sensor Kelembapan']) }}
			</div>
		</div>
		<div style="padding-bottom:50px;"></div>
	</div>

	<div class="col-md-12">
		<div class="form-group">
			<label class="col-md-2 control-label" for="sensor_warna">Sensor Warna</label>
			<div class="col-md-10">
				{{ Form::select('sensor_warna', ['KELABU' => 'Kelabu Hitam', 'MERAH' => 'Merah', 'KUNING' => 'Kuning'], null, ['class' => 'form-control','id' => 'sensor_warna','placeholder' => 'Sensor Warna']) }}
			</div>
		</div>
		<div style="padding-bottom:50px;"></div>
	</div>
	
	<div class="col-md-12">
	    <button type="submit" class="btn btn-primary btn-sm pull-right">
	      <i class="fa fa-save"></i> Simpan
	    </button>
	    <div style="padding-bottom:20px;"></div>
	</div>

</div>